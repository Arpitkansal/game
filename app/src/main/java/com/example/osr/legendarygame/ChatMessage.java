package com.example.osr.legendarygame;

/**
 * Created by osr on 26/2/18.
 */

public class ChatMessage {

    public final static String MSG_TYPE_SENT = "MSG_TYPE_SENT";
    public final static String MSG_TYPE_RCVD = "MSG_TYPE_RCVD";

    private String msgContent, msgType;

    public ChatMessage(String msgType, String msgContent) {
        this.msgType = msgType;
        this.msgContent = msgContent;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

}
