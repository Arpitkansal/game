package com.example.osr.legendarygame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by osr on 26/2/18.
 */

public class ExpandableListData {

    public static HashMap<String, List<String>> getData()
    {
        HashMap<String, List<String >> expandableListDetail= new HashMap<String, List<String >>();

        List<String> legendary= new ArrayList<String>();
        legendary.add("text1");
        legendary.add("text2");

        List<String> rules=new ArrayList<String>();
        rules.add("text1");
        rules.add("text2");

        List<String> videoGames=new ArrayList<String>();

        videoGames.add("text1");
        videoGames.add("text2");

        List<String> systems=new ArrayList<String>();

        systems.add("text1");
        systems.add("text2");

        List<String> ageRequirement=new ArrayList<String>();

        ageRequirement.add("text1");
        ageRequirement.add("text2");

        List<String> monthlyFee=new ArrayList<String>();

        monthlyFee.add("text1");
        monthlyFee.add("text2");

        List<String> costToCompete=new ArrayList<String>();

        costToCompete.add("text1");
        costToCompete.add("text2");

        List<String> disputesHandled=new ArrayList<String>();

        disputesHandled.add("text1");
        disputesHandled.add("text2");


        expandableListDetail.put("What is Legendary Gamers", legendary);
        expandableListDetail.put("What are the rules", rules);
        expandableListDetail.put("What video games does Legendary Gamers support", videoGames);
        expandableListDetail.put("What systems/consoles to Legendary Gamers support", systems);
        expandableListDetail.put("Is there any age requirement to join Legendary Gamers", ageRequirement);
        expandableListDetail.put("Are there any monthly fees", monthlyFee);
        expandableListDetail.put("How much does it cost to compete", costToCompete);
        expandableListDetail.put("How are disputes handled", disputesHandled);

        return expandableListDetail;

    }
}
