package com.example.osr.legendarygame;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.osr.legendarygame.databinding.ActivitySupportBinding;

public class ActivitySupport extends AppCompatActivity {

    RelativeLayout rLayoutDispute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySupportBinding activitySupportBinding= DataBindingUtil.setContentView
                (this, R.layout.activity_support);
        rLayoutDispute=findViewById(R.id.relative_layout1);

        rLayoutDispute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(ActivitySupport.this, ActivityDispute.class);
                startActivity(intent);

            }
        });
    }
}
