package com.example.osr.legendarygame;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by osr on 25/1/18.
 */

public class AnimatedDialog extends AppCompatActivity {

    final Context context=this;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bounce);
        animateButton();

    }
    void animateButton() {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        double animationDuration =  500;
        myAnim.setDuration((long)animationDuration);

        BounceInterpolator interpolator = new BounceInterpolator(1, 20);
        myAnim.setInterpolator(interpolator);
        AlertDialog.Builder builder=new AlertDialog.Builder(AnimatedDialog.this);
        builder.setView(R.layout.dialog);

//        TextView mymsg=new TextView(this);

//        builder.setTitle("Whoops!");
//        mymsg.setGravity(Gravity.CENTER);
//        builder.setView(mymsg);
//        builder.setMessage("Your phone number is not registered. Please contact us");
//        builder.setPositiveButton("OK",null);
        AlertDialog dialog=builder.create();
        dialog.getWindow(). getAttributes().windowAnimations = R.style.Theme_Dialog;
        myAnim.start();
        dialog.show();
    }
    public void cotinue(View view)
    {
        Intent intent=new Intent(AnimatedDialog.this,IndicatorActivity.class);
        startActivity(intent);
    }
}
