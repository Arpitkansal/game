package com.example.osr.legendarygame;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.osr.legendarygame.databinding.ActivityFaqBinding;

public class ActivityFaq extends AppCompatActivity {

    RelativeLayout rLayout_about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityFaqBinding activityFaqBinding= DataBindingUtil.setContentView
                (this,R.layout.activity_faq);
        rLayout_about=findViewById(R.id.relative_layout);
        rLayout_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActivityFaq.this, ActivityAbout.class);
                startActivity(intent);
            }
        });

    }
}
