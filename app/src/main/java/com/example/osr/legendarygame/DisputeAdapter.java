package com.example.osr.legendarygame;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by osr on 23/2/18.
 */

public class DisputeAdapter extends RecyclerView.Adapter<DisputeAdapter.DViewHolder> {
    Context context;

    DisputeAdapter(Context context){
        this.context=context;
    }

    @Override
    public DisputeAdapter.DViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.content_items,parent,false);
        return new DisputeAdapter.DViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DViewHolder holder, int position) {

        holder.rLayout_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(context,ActivityDisputeDetails.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class DViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rLayout_item;
        public DViewHolder(View itemView) {
            super(itemView);
            rLayout_item = itemView.findViewById(R.id.relative_layout);

        }
    }
}
