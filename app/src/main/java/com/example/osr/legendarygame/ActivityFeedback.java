package com.example.osr.legendarygame;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.osr.legendarygame.databinding.ActivityFeedbackBinding;

public class ActivityFeedback extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityFeedbackBinding activityFeedbackBinding= DataBindingUtil.setContentView
                (this,R.layout.activity_feedback);
    }
}
