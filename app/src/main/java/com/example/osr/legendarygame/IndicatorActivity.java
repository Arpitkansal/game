package com.example.osr.legendarygame;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by osr on 29/1/18.
 */

public class IndicatorActivity extends AppCompatActivity {
    private AVLoadingIndicatorView avi;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indicator);
        avi=(AVLoadingIndicatorView)findViewById(R.id.avi);
        avi.setIndicator("com.example.osr.legendarygame.MyCustomIndicator");
        avi.smoothToShow();
//        avi.setIndicatorColor(R.color.dull_orange);

    }
    public void cotinue(View view)
    {
        Intent intent=new Intent(IndicatorActivity.this,SelectFile.class);
        startActivity(intent);
    }


}
