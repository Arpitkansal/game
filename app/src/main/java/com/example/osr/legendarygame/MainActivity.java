package com.example.osr.legendarygame;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.osr.legendarygame.databinding.ActivityMainBinding;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;

public class MainActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR  = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS     = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION              = 200;

    private boolean mIsTheTitleVisible          = false;
    private boolean mIsTheTitleContainerVisible = true;

    private LinearLayout mTitleContainer;
    private TextView mTitle;
    private AppBarLayout mAppBarLayout;
    private Toolbar mToolbar;
    private TabLayout tabs;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding activityMainBinding= DataBindingUtil.setContentView
                (this,R.layout.activity_main);
        mAppBarLayout = activityMainBinding.mainAppBar;
        mAppBarLayout.addOnOffsetChangedListener(this);
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        final ActionBar ab = getSupportActionBar();

        //ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        tabs = (TabLayout) findViewById(R.id.tab_layout);
        ViewPager viewPager=(ViewPager) findViewById(R.id.viewpager);


    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout,int offset)
    {
        int maxScroll=appBarLayout.getTotalScrollRange();
        float percentage=(float)Math.abs(offset)/(float)maxScroll;

        //handleAlphaOnTitle(percentage);
        //handleToolbarTitleVisibility(percentage);
    }



    private void handleToolbarTitleVisibility(float percentage) {

        if(percentage>=PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR)
        {
            if(mIsTheTitleVisible)
            {
                startAlphaAnimation(mTitle,ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible=true;
            }
            else
            {
                if(mIsTheTitleVisible)
                {
                    startAlphaAnimation(mTitle,ALPHA_ANIMATIONS_DURATION,View.INVISIBLE);
                    mIsTheTitleVisible=false;
                }


            }
        }

    }
    private void handleAlphaOnTitle(float percentage) {

        if(percentage>=PERCENTAGE_TO_HIDE_TITLE_DETAILS)
        {
            if (mIsTheTitleContainerVisible)
            {
                startAlphaAnimation(mTitleContainer,ALPHA_ANIMATIONS_DURATION,View.INVISIBLE);
                mIsTheTitleContainerVisible=false;

            }
        }
        else
        {
            if(!mIsTheTitleContainerVisible)
            {
                startAlphaAnimation(mTitleContainer,ALPHA_ANIMATIONS_DURATION,View.INVISIBLE);
                mIsTheTitleContainerVisible=true;
            }
        }
    }
    public static void startAlphaAnimation (View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    public void cotinue(View view)
    {
        Intent intent=new Intent(MainActivity.this,CountryPicker.class);
        startActivity(intent);
    }



}
