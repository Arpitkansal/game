package com.example.osr.legendarygame;

import android.databinding.DataBindingUtil;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.osr.legendarygame.databinding.ActivityDisputeBinding;

public class ActivityDispute extends AppCompatActivity {

    RecyclerView recyclerView_dispute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityDisputeBinding activityDisputeBinding= DataBindingUtil.setContentView
                (this,R.layout.activity_dispute);

        recyclerView_dispute=activityDisputeBinding.recyclerViewDispute;
        DisputeAdapter disputeAdapter=new DisputeAdapter(getApplicationContext());
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(this);
        recyclerView_dispute.setLayoutManager(layoutManager);
        recyclerView_dispute.setAdapter(disputeAdapter);
    }
}
