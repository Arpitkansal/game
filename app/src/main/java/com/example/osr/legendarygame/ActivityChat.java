package com.example.osr.legendarygame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class ActivityChat extends AppCompatActivity {
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        final EditText editText_msg=findViewById(R.id.et_input);
        Button btn_send=findViewById(R.id.button_send);

        final RecyclerView recyclerView=findViewById(R.id.chat_recyclerView);

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        final List<ChatMessage> chatMessagesList=new ArrayList<ChatMessage>();
        final ChatMessage chatMessage=new ChatMessage(ChatMessage.MSG_TYPE_RCVD,"Hi");
        chatMessagesList.add(chatMessage);

        final ChatAdapter chatAdapter=new ChatAdapter(chatMessagesList);
        recyclerView.setAdapter(chatAdapter);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msgContent=editText_msg.getText().toString();

                if(!TextUtils.isEmpty(msgContent))
                {
                    ChatMessage chatMessage= new ChatMessage(ChatMessage.MSG_TYPE_SENT,msgContent);
                    chatMessagesList.add(chatMessage);
                    int newMsgPstn=chatMessagesList.size()-1;
                    chatAdapter.notifyItemInserted(newMsgPstn);
                    recyclerView.scrollToPosition(newMsgPstn);
                    editText_msg.setText("");
                }
            }
        });
    }
}
