package com.example.osr.legendarygame;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by osr on 26/2/18.
 */

public class ChatViewHolder extends RecyclerView.ViewHolder {

    LinearLayout leftLayout, rightLayout;
    TextView leftTextView, rightTextView;

    public ChatViewHolder(View itemView) {
        super(itemView);

        if (itemView != null) {
            leftLayout = (LinearLayout) itemView.findViewById(R.id.linear_layoutLeft);
            rightLayout = (LinearLayout) itemView.findViewById(R.id.linear_layoutRight);
            leftTextView = itemView.findViewById(R.id.tv_left);
            rightTextView = itemView.findViewById(R.id.tv_right);
        }
    }
}
