package com.example.osr.legendarygame;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.osr.legendarygame.databinding.ActivityBalanceBinding;

/**
 * Created by osr on 24/1/18.
 */

public class Balance extends AppCompatActivity {
    RecyclerView recyclerView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBalanceBinding activityMainBinding= DataBindingUtil.setContentView
                (this,R.layout.activity_balance);
    recyclerView=activityMainBinding.recyclerView;
    Radapter radapter=new Radapter(getApplicationContext());

    RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(radapter);

    }

    public void cotinue(View view)
    {
        Intent intent=new Intent(Balance.this,AnimatedDialog.class);
        startActivity(intent);
    }



}
