package com.example.osr.legendarygame;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by osr on 30/1/18.
 */

public class MyServ extends Service {

    private final IBinder binder=new LocalBinder();

    public class LocalBinder extends Binder{

        MyServ getService()
        {
            return MyServ.this;
        }

    }
    @Override
    public IBinder onBind(Intent intent) {

        return binder;
    }

    public Date getCurrentDate()
    {
        return Calendar.getInstance().getTime();
    }
}
