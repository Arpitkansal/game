package com.example.osr.legendarygame;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.net.URI;

public class SelectFile extends AppCompatActivity {
    Intent intent;
    private static int IMG_RESULT = 1;
    String ImageDecode;
    String[] FILE;
    Bitmap bitmap;
    ImageView showImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_file);
        ImageView addImage=(ImageView)findViewById(R.id.imageView);
        ImageView showImage=(ImageView)findViewById(R.id.imageView1);

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }
    public void selectImage() {
        final CharSequence[] options = {"Take Photo from Camera", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(SelectFile.this);
        builder.setTitle("Add photo");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int items) {

                if (options[items].equals("Take Photo from Camera")) {
                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);
                } else if (options[items].equals("Choose from Gallery")) {
                    Intent intent1 = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent1, 2);
                } else if (options[items].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 1 && resultCode == RESULT_OK) {
//
//            bitmap = (Bitmap) data.getExtras().get("data");
//            showImage.setImageBitmap(bitmap);
//
//        } else if (requestCode == 2 && resultCode == RESULT_OK) {
//
//            Uri selectedImage = data.getData();
//
//            showImage.setImageURI(selectedImage);
//
//        }
//
//    }
    public void delete(View view)
    {
        showImage.setImageDrawable(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        super.onActivityResult(requestCode,resultCode,data);

        try{
            if(requestCode==IMG_RESULT &&resultCode==RESULT_OK && null!=data)
            {
               Uri uri =data.getData();


                ImageView showImage=(ImageView)findViewById(R.id.imageView1);
                showImage.setImageURI(uri);
//                showImage.setImageBitmap(BitmapFactory.decodeFile(ImageDecode));


            }
        }catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }
    }
}
