package com.example.osr.legendarygame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class PickYourSystem extends AppCompatActivity {

    RelativeLayout relativeLayout_xbox, relativeLayout_ps4;
    ImageView imageView_tick_xbox,imageView_tick_ps4;
    Button button_next;
    boolean xbox, ps4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_your_system);
        relativeLayout_xbox = findViewById(R.id.relative_xbox);
        relativeLayout_ps4=findViewById(R.id.relative_ps4);
        button_next=findViewById(R.id.button_next);
        imageView_tick_xbox=findViewById(R.id.iv_tick_xbox);
        imageView_tick_ps4=findViewById(R.id.iv_tick_ps4);
        relativeLayout_xbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_next.setBackgroundColor(getResources().getColor(R.color.dull_orange));

                relativeLayout_xbox.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                imageView_tick_xbox.setVisibility(View.VISIBLE);
                if (!ps4 && !xbox){
                    xbox=true;
                } else if (ps4){
                    relativeLayout_ps4.setBackgroundColor(getResources().getColor(R.color.trans));
                    imageView_tick_ps4.setVisibility(View.INVISIBLE);
                    xbox=true;
                    ps4 = false;
                }
                /*if(ps4!=true) {
                    relativeLayout_ps4.setBackgroundColor(getResources().getColor(R.color.trans));
                    imageView_tick_ps4.setVisibility(View.INVISIBLE);
                    relativeLayout_xbox.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                    imageView_tick_xbox.setVisibility(View.VISIBLE);
                    button_next.setBackgroundColor(getResources().getColor(R.color.dull_orange));
                    xbox=true;
                }
                else
                {

                }*/

            }
        });

        relativeLayout_ps4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button_next.setBackgroundColor(getResources().getColor(R.color.dull_orange));
                relativeLayout_ps4.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                imageView_tick_ps4.setVisibility(View.VISIBLE);
                ps4=true;
                if(!xbox && !ps4) {

                    ps4=true;

                } else if (xbox)
                {
                    relativeLayout_xbox.setBackgroundColor(getResources().getColor(R.color.trans));
                    imageView_tick_xbox.setVisibility(View.INVISIBLE);
                    ps4=true;
                    xbox = false;
                }
            }
        });

        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PickYourSystem.this,PickYourGame.class);
                startActivity(intent);
            }
        });

    }
}
