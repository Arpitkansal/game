package com.example.osr.legendarygame;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;

/**
 * Created by osr on 16/1/18.
 */

public class Spacenavigation extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.space_navigation);
        SpaceNavigationView spaceNavigationView = (SpaceNavigationView) findViewById(R.id.space);
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);
        spaceNavigationView.addSpaceItem(new SpaceItem("abc", R.drawable.ic_noun));
        spaceNavigationView.addSpaceItem(new SpaceItem("a", R.drawable.ic_noun));
        spaceNavigationView.addSpaceItem(new SpaceItem("a",R.drawable.ic_noun));
        spaceNavigationView.addSpaceItem(new SpaceItem("a",R.drawable.ic_noun));
        //spaceNavigationView.setFont(Typeface.createFromAsset(getAssets(), "your_cutom_font.ttf"));

//

    }

}
