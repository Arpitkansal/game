package com.example.osr.legendarygame;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by osr on 26/2/18.
 */

public class ChatAdapter extends RecyclerView.Adapter {

    List<ChatMessage> chatMessageList = null;
    ChatMessage chatMessages;
    View view;
    private static final int TYPE_ONE = 1;
    private static final int TYPE_TWO = 2;

    public ChatAdapter(List<ChatMessage> chatMessageList) {
        this.chatMessageList = chatMessageList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case 1:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_sent, parent, false);

                return new SentMessageHolder(view);

            case 2:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.msg_rcvd, parent, false);
                return new ReceivedMessageHolder(view);

        }

        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        chatMessages = this.chatMessageList.get(position);
        switch (holder.getItemViewType()) {
            case TYPE_ONE:
                initLayoutOne((SentMessageHolder) holder, position);
                break;
            case TYPE_TWO:
                initLayoutTwo((ReceivedMessageHolder) holder, position);
                break;
            default:
                break;
        }


//        if(chatMessages.MSG_TYPE_RCVD.equals(chatMessages.getMsgType()))
//        {
//            holder.leftLayout.setVisibility(LinearLayout.VISIBLE);
//        }
//        else if(chatMessages.MSG_TYPE_SENT.equals(chatMessages.getMsgType()))
//        {
//            holder.rightLayout.setVisibility(LinearLayout.VISIBLE);
//        }

    }

    private void initLayoutOne(SentMessageHolder holder, int pos) {
        holder.rightLayout.setVisibility(View.VISIBLE);
    }

    private void initLayoutTwo(ReceivedMessageHolder holder, int pos) {
        holder.leftLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemViewType(int position) {
        chatMessages = this.chatMessageList.get(position);
        if (chatMessages.getMsgType() == ChatMessage.MSG_TYPE_SENT) {
            return TYPE_ONE;
        } else if (chatMessages.getMsgType() == ChatMessage.MSG_TYPE_RCVD)
            return TYPE_TWO;
        else {
            return -1;
        }
    }

    @Override
    public int getItemCount() {

        if (chatMessageList == null) {
            chatMessageList = new ArrayList<ChatMessage>();
        }
        return chatMessageList.size();
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText;
        LinearLayout rightLayout;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.tv_right);
            rightLayout = (LinearLayout) itemView.findViewById(R.id.linear_layoutRight);

        }

        void bind(ChatMessage message) {
            rightLayout.setVisibility(View.VISIBLE);
            messageText.setText(message.getMsgContent());

        }
    }

    class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText;
        LinearLayout leftLayout;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.tv_left);
            leftLayout = (LinearLayout) itemView.findViewById(R.id.linear_layoutLeft);

        }

        void bind(ChatMessage message) {
            leftLayout.setVisibility(View.VISIBLE);
            messageText.setText(message.getMsgContent());

        }
    }


}
