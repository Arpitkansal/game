package com.example.osr.legendarygame;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.osr.legendarygame.databinding.ActivityDisputeDetailsBinding;

public class ActivityDisputeDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityDisputeDetailsBinding activityDisputeDetailsBinding= DataBindingUtil.
                setContentView(this,R.layout.activity_dispute_details);
    }

    public void cotinue(View view)
    {
        Intent intent=new Intent(ActivityDisputeDetails.this,ActivityDisputeText.class);
        startActivity(intent);
    }
}
