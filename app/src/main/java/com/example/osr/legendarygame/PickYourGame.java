package com.example.osr.legendarygame;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.example.osr.legendarygame.databinding.ActivityMainBinding;
import com.example.osr.legendarygame.databinding.ActivityPickYourGameBinding;

public class PickYourGame extends AppCompatActivity {
    RecyclerView recyclerView;
    Button btn_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ActivityPickYourGameBinding activityPickYourGameBinding= DataBindingUtil.setContentView
                (this,R.layout.activity_pick_your_game);
        recyclerView= activityPickYourGameBinding.recyclerViews;
        Radapter radapter=new Radapter(getApplicationContext());
        RecyclerView.LayoutManager layoutManager= new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(radapter);

    }

    public void cotinue(View view)
    {
        Intent intent=new Intent(PickYourGame.this,GameMode.class);
        startActivity(intent);

    }
}
