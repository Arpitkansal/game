package com.example.osr.legendarygame;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.osr.legendarygame.databinding.ActivityAboutBinding;

public class ActivityAbout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityAboutBinding activityAboutBinding= DataBindingUtil.setContentView
                (this,R.layout.activity_about);
    }
}
