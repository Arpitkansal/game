package com.example.osr.legendarygame;

/**
 * Created by osr on 25/1/18.
 */

public class BounceInterpolator implements android.view.animation.Interpolator {

    double mAmplitude=1;
    double mFrequency=10;

    public BounceInterpolator(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }
    @Override
    public float getInterpolation(float time) {

        double amplitude = mAmplitude;
        if (amplitude == 0)

        {
            amplitude = 1;
        }
        return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) * Math.cos(mFrequency * time) + 1);

    }
}
