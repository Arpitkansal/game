package com.example.osr.legendarygame;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.osr.legendarygame.databinding.GameModeBinding;

/**
 * Created by osr on 22/2/18.
 */

public class GameMode extends AppCompatActivity {

    ImageView iv_tick_headToHead,iv_tick_myPark,iv_tick_myTeam,iv_tick_proAm;
    RelativeLayout rLayout_headToHead,rLayout_myTeam,rLayout_myPark,rLayout_proAm;
    Button btn_next;
    boolean headToHead,myTeam,myPark,proAm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GameModeBinding gameModeBinding= DataBindingUtil.setContentView(this,R.layout.game_mode);
        iv_tick_headToHead=findViewById(R.id.iv_tick1);
        iv_tick_myTeam=findViewById(R.id.iv_tick2);
        iv_tick_myPark=findViewById(R.id.iv_tick3);
        iv_tick_proAm=findViewById(R.id.iv_tick4);
        rLayout_headToHead=findViewById(R.id.relative_layout2);
        rLayout_myTeam=findViewById(R.id.relative_layout3);
        rLayout_myPark=findViewById(R.id.relative_layout4);
        rLayout_proAm=findViewById(R.id.relative_layout5);
        btn_next=findViewById(R.id.button_next);


        rLayout_headToHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_next.setBackgroundColor(getResources().getColor(R.color.dull_orange));
                iv_tick_headToHead.setVisibility(View.VISIBLE);
                if(!headToHead || !myTeam || !myPark || !proAm)
                {
                    headToHead=true;
                }
                else if (myTeam)
                {
                    iv_tick_myTeam.setVisibility(View.GONE);
                    myTeam=false;
                    headToHead=true;
                }
                else if(myPark)
                {
                    iv_tick_myPark.setVisibility(View.GONE);
                    myPark=false;
                    headToHead=true;
                }
                else if(proAm)
                {
                    iv_tick_proAm.setVisibility(View.GONE);
                    proAm=false;
                    headToHead=true;
                }
            }
        });

        rLayout_myTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_next.setBackgroundColor(getResources().getColor(R.color.dull_orange));
                iv_tick_myTeam.setVisibility(View.VISIBLE);
                if(!headToHead || !myTeam || !myPark || !proAm)
                {
                    myTeam=true;
                }
                else if (headToHead)
                {
                    iv_tick_headToHead.setVisibility(View.GONE);
                    headToHead=false;
                    myTeam=true;
                }
                else if(myPark)
                {
                    iv_tick_myPark.setVisibility(View.GONE);
                    myPark=false;
                    myTeam=true;
                }
                else if(proAm)
                {
                    iv_tick_proAm.setVisibility(View.GONE);
                    proAm=false;
                    myTeam=true;
                }

            }
        });

        rLayout_myPark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_next.setBackgroundColor(getResources().getColor(R.color.dull_orange));
                iv_tick_myPark.setVisibility(View.VISIBLE);
                if(!headToHead || !myTeam || !myPark || !proAm)
                {
                    myPark=true;
                }
                else if (headToHead)
                {
                    iv_tick_headToHead.setVisibility(View.GONE);
                    headToHead=false;
                    myPark=true;
                }
                else if(myTeam)
                {
                    iv_tick_myPark.setVisibility(View.GONE);
                    myTeam=false;
                    myPark=true;
                }
                else if(proAm)
                {
                    iv_tick_proAm.setVisibility(View.GONE);
                    proAm=false;
                    myPark=true;
                }

            }
        });

        rLayout_proAm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_next.setBackgroundColor(getResources().getColor(R.color.dull_orange));
                iv_tick_proAm.setVisibility(View.VISIBLE);
                if(!headToHead || !myTeam || !myPark || !proAm)
                {
                    proAm=true;
                }
                else if (headToHead)
                {
                    iv_tick_headToHead.setVisibility(View.GONE);
                    headToHead=false;
                    proAm=true;
                }
                else if(myTeam)
                {
                    iv_tick_myPark.setVisibility(View.GONE);
                    myTeam=false;
                    proAm=true;
                }
                else if(myPark)
                {
                    iv_tick_proAm.setVisibility(View.GONE);
                    myPark=false;
                    proAm=true;
                }

            }
        });

    }
}
