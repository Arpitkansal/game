package com.example.osr.legendarygame;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


public class Radapter extends RecyclerView.Adapter<Radapter.RViewHolder>{
    Context context;

    Radapter(Context context){
        this.context=context;
    }

    @Override
    public RViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.content_item,parent,false);
        return new RViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RViewHolder holder, int position) {

        holder.imageViewAbc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }


    @Override
    public int getItemCount() {
        return 10;
    }

    class RViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewAbc;
        public RViewHolder(View itemView) {
            super(itemView);
            imageViewAbc = itemView.findViewById(R.id.iv_phn);
        }
    }
}
