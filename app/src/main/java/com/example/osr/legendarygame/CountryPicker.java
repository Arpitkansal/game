package com.example.osr.legendarygame;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import java.util.Locale;


public class CountryPicker extends AppCompatActivity {
    EditText et1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.country_picker);

        et1 = (EditText) findViewById(R.id.edittext);
        et1.addTextChangedListener(new TextWatcher() {
            private boolean mFormatting; // a flag that prevents stack overflows.
            private int mAfter;

            @Override
            public void onTextChanged(CharSequence s, int cursorPosition, int before,
                                      int count) {

                if (before == 0 && count == 1) {

                    String val = s.toString();
                    String a = "";
                    String b = "";
                    String c = "";
                    if (val != null && val.length() > 0) {
                        val = val.replace("-", "");
                        if (val.length() >= 3) {
                            a = val.substring(0, 3);
                        } else if (val.length() < 3) {
                            a = val.substring(0, val.length());
                        }
                        if (val.length() >= 6) {
                            b = val.substring(3, 6);
                            c = val.substring(6, val.length());
                        } else if (val.length() > 3 && val.length() < 6) {
                            b = val.substring(3, val.length());
                        }
                        StringBuffer stringBuffer = new StringBuffer();
                        if (a != null && a.length() > 0) {
                            stringBuffer.append(a);

                        }
                        if (b != null && b.length() > 0) {
                            stringBuffer.append("-");
                            stringBuffer.append(b);

                        }
                        if (c != null && c.length() > 0) {
                            stringBuffer.append("-");
                            stringBuffer.append(c);
                        }
                        et1.removeTextChangedListener(this);
                        et1.setText(stringBuffer.toString());
                        if (cursorPosition == 3 || cursorPosition == 7) {
                            cursorPosition = cursorPosition + 2;
                        } else {
                            cursorPosition = cursorPosition + 1;
                        }
                        if (cursorPosition <= et1.getText().toString().length()) {
                            et1.setSelection(cursorPosition);
                        } else {
                            et1.setSelection(et1.getText().toString().length());
                        }
                        et1.addTextChangedListener(this);
                    } else {
                        et1.removeTextChangedListener(this);
                        et1.setText("");
                        et1.addTextChangedListener(this);
                    }

                }

                if (before == 1 && count == 0) {

                    String val = s.toString();
                    String a = "";
                    String b = "";
                    String c = "";

                    if (val != null && val.length() > 0) {
                        val = val.replace("-", "");
                        if (cursorPosition == 3) {
                            val = removeCharAt(val, cursorPosition - 1, s.toString().length() - 1);
                        } else if (cursorPosition == 7) {
                            val = removeCharAt(val, cursorPosition - 2, s.toString().length() - 2);
                        }
                        if (val.length() >= 3) {
                            a = val.substring(0, 3);
                        } else if (val.length() < 3) {
                            a = val.substring(0, val.length());
                        }
                        if (val.length() >= 6) {
                            b = val.substring(3, 6);
                            c = val.substring(6, val.length());
                        } else if (val.length() > 3 && val.length() < 6) {
                            b = val.substring(3, val.length());
                        }
                        StringBuffer stringBuffer = new StringBuffer();
                        if (a != null && a.length() > 0) {
                            stringBuffer.append(a);

                        }
                        if (b != null && b.length() > 0) {
                            stringBuffer.append("-");
                            stringBuffer.append(b);

                        }
                        if (c != null && c.length() > 0) {
                            stringBuffer.append("-");
                            stringBuffer.append(c);
                        }
                        et1.removeTextChangedListener(this);
                        et1.setText(stringBuffer.toString());
                        if (cursorPosition == 3 || cursorPosition == 7) {
                            cursorPosition = cursorPosition - 1;
                        }
                        if (cursorPosition <= et1.getText().toString().length()) {
                            et1.setSelection(cursorPosition);
                        } else {
                            et1.setSelection(et1.getText().toString().length());
                        }
                        et1.addTextChangedListener(this);
                    } else {
                        et1.removeTextChangedListener(this);
                        et1.setText("");
                        et1.addTextChangedListener(this);
                    }

                }


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {


            }

            public String removeCharAt(String s, int pos, int length) {

                String value = "";
                if (length > pos) {
                    value = s.substring(pos + 1);
                }
                return s.substring(0, pos) + value;
            }
        });


    }

    public void cotinue(View view)
    {
        Intent intent=new Intent(CountryPicker.this,Balance.class);
        startActivity(intent);
    }
}








